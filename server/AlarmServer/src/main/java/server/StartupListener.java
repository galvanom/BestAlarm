package server;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.*;

public class StartupListener implements ServletContextListener {
//    private Settings settings;
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // Initialize board service
        Settings settings = server.Settings.getInstance();
        String logFileLocation = settings.getValue("logfile");
        System.out.println("Path is " + Paths.get(".").toAbsolutePath().normalize().toString());
        try{
            Handler fh = new FileHandler(logFileLocation);
            Logger.getLogger("").addHandler(fh);
            Logger.getLogger("").setLevel(Level.INFO);
            fh.setFormatter(new SimpleFormatter());
        }
        catch (IOException e){
            System.out.printf("Can't open/create log file. %s", e);
        }
    }
    private void logInit(){

    }
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
