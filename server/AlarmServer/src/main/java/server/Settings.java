package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Settings {
    private static Settings instance;
    private final static String settingsFile = "settings.dat";
    private final Map<String, String> propertiesMap;

    synchronized public static Settings getInstance(){
        if (instance == null){
            instance = new Settings();
        }
        return instance;
    }

    /**
     * Loads all properties to map
     */
    private Settings(){
        Properties properties = new Properties();
        propertiesMap = new HashMap<String, String>();

        try{
            InputStream inputStream = new FileInputStream(new File(settingsFile));
            properties.load(inputStream);
            Set<String> keySet = properties.stringPropertyNames();
            for (String key : keySet){
                String value = properties.getProperty(key);
                propertiesMap.put(key, value);
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }
    public String getValue(String key){
        return propertiesMap.get(key);
    }
}
