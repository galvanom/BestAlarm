package server;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.io.DataInputStream;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.logging.Logger;
import java.util.logging.Handler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

import server.FileManager;
import server.FileManager.NewsProvider;
import server.texttospeech.ivona.TTSIvona.VoiceName;

public class TTSServer{
	private int serverPort;
	private FileManager fileManager;
	private static Logger log = Logger.getLogger(TTSServer.class.getName());
	private static Settings settings = server.Settings.getInstance();

	private static void logInit(){
	    String logFileLocation = settings.getValue("logfile");
		try{
			Handler fh = new FileHandler(logFileLocation);
			Logger.getLogger("").addHandler(fh);
			Logger.getLogger("").setLevel(Level.INFO);
			fh.setFormatter(new SimpleFormatter());
		}
		catch (IOException e){
			System.out.printf("Can't open/create log file. %s", e);
		}

	}

	public static void main(String[] args){
		logInit();

		new TTSServer().connect();
	}
	TTSServer(){
        serverPort = Integer.valueOf(settings.getValue("serverport"));
		fileManager = new FileManager(settings.getValue("audiopath"));
	}
	public void connect(){
		ServerSocket socket = null;
		Socket connection;
		BufferedReader in;
		PrintStream out;

 		try{
			socket = new ServerSocket(this.serverPort);
		}
		catch (IOException e){
			log.log(Level.SEVERE, "TTS Server could'n connect to port "
				+ String.valueOf(this.serverPort), e);
			// System.out.printf("Couldnt connect to port %d \n %s", this.serverPort, e);
		}
		log.log(Level.INFO, "Server started on "+this.serverPort+" port");
		if (socket != null){
			while (true){
				try{
					connection = socket.accept();
					// System.out.println("Connection established\n");
					in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					out = new PrintStream(connection.getOutputStream());

					while(connection.isBound()){
						String line = (String) in.readLine();
						if (line != null){
							// System.out.println(line);
							//TODO: Split and test them
							out.println(getAnswer(line));
						}

						connection.close();
					}

				}
				catch (SocketException e){
					log.log(Level.FINE, "TTS Server socketexception", e);
				}
				catch (IOException e){
					log.log(Level.SEVERE, "TTS Server couldn't communicate ", e);
					// System.out.println(e);
				}
			}
		}

 	}
 	private String getAnswer(String jsonString){
		log.info("Client request: " + jsonString);

 		String answer = "";
 		try {
	 		JSONParser parser = new JSONParser();
	 		JSONObject jsonObj = (JSONObject) parser.parse(jsonString);
	 		String type = (String)jsonObj.get("type");

	 		if (type.equals("news")){
	 		 	String provider = (String)jsonObj.get("provider");
	 			String voice = (String)jsonObj.get("voice");

	 		 	VoiceName voiceName = VoiceName.fromString(voice);
		 		NewsProvider newsProvider = NewsProvider.fromString(provider);

				// 	System.out.printf("Client request: %s %s %s\n", type, provider, voice);

		 		if (voiceName != null && newsProvider != null){
		 			answer = fileManager.getNewsFileName(newsProvider, voiceName);
		 		}
		 	}
		 	if (type.equals("gps")){
		 		double longtitude = Double.parseDouble((String)jsonObj.get("long"));
		 		double latitude = Double.parseDouble((String)jsonObj.get("lat"));
		 		String voice = (String)jsonObj.get("voice");

		 		VoiceName voiceName = VoiceName.fromString(voice);

				// 	System.out.printf("Client request: %s %f %f %s\n", type, longtitude, latitude, voice);

		 		if (voiceName != null){
		 			answer = fileManager.getWeatherFileName(longtitude, latitude, voiceName);
		 		}
		 	}

 		}
 		catch (ParseException e){
			log.log(Level.WARNING, "TTS Server could'n parse string: " + jsonString , e);
 		// 	System.out.println(e);
 		}
		log.info("FileManager answer: " + answer);
 	// 	System.out.printf("FileManager answer: %s\n", answer);
 		return answer;
 	}

}
