package server;

import server.texttospeech.ivona.TTSIvona;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;
import java.util.logging.*;

public class MainServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(MainServlet.class.getName());
    private static Settings settings = server.Settings.getInstance();
    private FileManager fileManager;

    MainServlet(){
        fileManager = new FileManager(settings.getValue("audiopath"));
    }
//{"type":"news", "provider":"yandex", "voice":"maxim"}
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> parameters = req.getParameterMap();
        try {
            String answer = getAnswer(parameters);
            System.out.println("Answer: " + answer);
            if (answer.length() > 0){
                sendFile(answer, resp);
            }
        }
        catch (IllegalArgumentException e){
            log.info("Illegal parameters in GET request");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    private void sendFile(String filename, HttpServletResponse resp){
        String fullPath = settings.getValue("audiopath") + "/" + filename;
        File file = new File(fullPath);
        resp.setHeader("Content-Disposition", "filename=\"" + filename + "\"");
        resp.setContentType("audio/mpeg");
        resp.setContentLength((int)file.length());
        FileInputStream input = null;
        ServletOutputStream output = null;

        try {
            input = new FileInputStream(file);
            output = resp.getOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead;
            while((bytesRead = input.read(buffer)) != -1){
                output.write(buffer, 0, bytesRead);
            }
        }
        catch(IOException e){
            log.log(Level.INFO, "Can't open stream");
        }
        finally {
            try {
                if (input != null)
                    input.close();
                if (output != null)
                    output.close();
            }
            catch(IOException e){
                log.log(Level.INFO, "Can't close stream");
            }
        }

    }
    private String getAnswer(Map<String,String[]> request) throws IllegalArgumentException{
        String answer = "";
        if (!request.containsKey("type") || !request.containsKey("voice") ){
            throw new IllegalArgumentException("No type or voice parameter found");
        }
        String type = request.get("type")[0];
        String voice = request.get("voice")[0];

        if (type.equals("news")){
            if (!request.containsKey("provider")){
                throw new IllegalArgumentException("No provider parameter found");
            }
            String provider = request.get("provider")[0];

            TTSIvona.VoiceName voiceName = TTSIvona.VoiceName.fromString(voice);
            FileManager.NewsProvider newsProvider = FileManager.NewsProvider.fromString(provider);

            // 	System.out.printf("Client request: %s %s %s\n", type, provider, voice);

            if (voiceName != null && newsProvider != null){
                answer = fileManager.getNewsFileName(newsProvider, voiceName);
            }
        }
        if (type.equals("gps")){
            if (!request.containsKey("long") || !request.containsKey("lat") ){
                throw new IllegalArgumentException("No long or lat parameter found");
            }
            double longitude = Double.parseDouble(request.get("long")[0]);
            double latitude = Double.parseDouble(request.get("lat")[0]);

            TTSIvona.VoiceName voiceName = TTSIvona.VoiceName.fromString(voice);

            // 	System.out.printf("Client request: %s %f %f %s\n", type, longitude, latitude, voice);
            if (voiceName != null){
                answer = fileManager.getWeatherFileName(longitude, latitude, voiceName);
            }
        }

        log.info("FileManager answer: " + answer);
        return answer;
    }
}
