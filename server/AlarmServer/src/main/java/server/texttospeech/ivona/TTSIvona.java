package server.texttospeech.ivona;

import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import server.Settings;
import server.texttospeech.TextToSpeech;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.AssertionError;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.ivona.services.tts.IvonaSpeechCloudClient;
import com.ivona.services.tts.model.CreateSpeechRequest;
import com.ivona.services.tts.model.CreateSpeechResult;
import com.ivona.services.tts.model.Input;
import com.ivona.services.tts.model.Voice;

public class TTSIvona implements TextToSpeech{
	private static Logger log = Logger.getLogger(TTSIvona.class.getName());
	private Settings settings;
	private static IvonaSpeechCloudClient speechCloud;

    private static final String SERVER_DUBLIN = "https://tts.eu-west-1.ivonacloud.com";
    private static final String SERVER_VIRGINIA = "https://tts.us-east-1.ivonacloud.com";
    private static final String SERVER_OREGON = "https://tts.us-west-2.ivonacloud.com";

    // private static final String VOICE_MAXIM = "Maxim";
    // private static final String VOICE_TATYANA = "Tatyana";
    // private String voice;

    public enum VoiceName{
		Tatyana, Maxim;

		public String toString(){
			String str = "";
				switch(this){
					case Tatyana:
						str = "Tatyana";
						break;
					case Maxim:
						str = "Maxim";
						break;
					default:
				}
			return str;
		}
		public static VoiceName fromString(String str){
			VoiceName voiceName = null;
				switch(str){
					case "tatyana":
						voiceName = Tatyana;
						break;
					case "maxim":
						voiceName = Maxim;
						break;
					default:
				}
			return voiceName;

		}
	}

	public TTSIvona(){
    	settings = Settings.getInstance();
		String accessKeysPath = null;
    	if (settings != null){
			accessKeysPath = settings.getValue("ivonacred");
			if (accessKeysPath != null) {
				speechCloud = new IvonaSpeechCloudClient(
						new PropertiesFileCredentialsProvider(accessKeysPath));

				speechCloud.setEndpoint(SERVER_OREGON);
			}
			else {
				log.log(Level.SEVERE, "Couldn't get Ivona credentials from settings");
			}
		}
		else {
    		log.log(Level.SEVERE, "Couldn't load settings");
		}
	}
	/*
	* Takes text and get speech from Ivona
	* Then saves file to filePath
	*/
	public boolean getSpeech(String text, VoiceName voiceName, String filePath){
		CreateSpeechRequest speechRequest = new CreateSpeechRequest();
		CreateSpeechResult speechResult;
		InputStream audioFileFromServer;
		FileOutputStream audioFileLocal;
		Input input = new Input();
        Voice voice = new Voice();

		String strVoiceName = voiceName.toString();
		if (strVoiceName == null){
			log.log(Level.SEVERE, "Unknown voice name.");
			return false;
		}
        voice.setName(voiceName.toString());

		input.setData(text);
        input.setType("application/ssml+xml");
        speechRequest.setInput(input);
        speechRequest.setVoice(voice);

        speechResult = speechCloud.createSpeech(speechRequest);
		audioFileFromServer = speechResult.getBody();
		if (audioFileFromServer == null){
			log.log(Level.SEVERE, "Can't get speech body");
			return false;
		}
		try {
			System.out.println("File path " + filePath);
			audioFileLocal = new FileOutputStream(new File(filePath));
			byte[] buffer = new byte[2 * 1024];
			int readBytes;

			while ((readBytes = audioFileFromServer.read(buffer)) > 0) {
				audioFileLocal.write(buffer, 0, readBytes);
			}
		}
		catch (IOException e){
			log.log(Level.WARNING, "Couldnt get file from Ivona" + e);
			// System.out.println(e);
			return false;
		}
		return true;

	}


}
