package server.texttospeech;

import server.texttospeech.ivona.TTSIvona.VoiceName;

public interface TextToSpeech{
	public boolean getSpeech(String text, VoiceName voiceName, String filePath);
}
