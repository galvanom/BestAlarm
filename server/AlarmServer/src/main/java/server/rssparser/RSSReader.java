package server.rssparser;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.EndElement;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class RSSReader{
	private static Logger log = Logger.getLogger(RSSReader.class.getName());

	final String ITEM = "item";
	final String TITLE = "title";
	final String GUID = "guid";
	final String LINK = "link";
	final String PUBDATE = "pubdate";
	final String DESCRIPTION = "description";

	public Feed readFromURL(String url){
		InputStream in;
		Feed feed = new Feed();
		try{
			URL remoteRSS = new URL(url);
			in = remoteRSS.openStream();
			feed = parseStream(in);
			in.close();
		}
		catch(MalformedURLException e){
			log.log(Level.WARNING, "Bad RSS URL. ", e);
			// System.out.println(e);
		}
		catch(IOException e){
			log.log(Level.WARNING, "Couldnt read RSS. ", e);
			// System.out.println(e);
		}

		return feed;
	}
	public Feed readFromFile(String fileName){
		InputStream in;
		Feed feed = new Feed();
		try {
			in = new FileInputStream(fileName);
			feed = parseStream(in);
			in.close();
		}
		catch(IOException e) {
			System.out.printf("Can't read RSS from file %s. %s", fileName, e);
		}

		return feed;
	}
	protected Feed parseStream(InputStream in){
		Feed feed = new Feed();
		FeedMessage message = new FeedMessage();
		boolean isWithinTitle = false;
		try{
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLEventReader reader = factory.createXMLEventReader(in);
			while(reader.hasNext()){
				XMLEvent event = reader.nextEvent();
				// System.out.printf("%s", event.toString());
				if (event.isStartElement()){
					StartElement startElement = event.asStartElement();
					String startElementName = startElement.getName()
						.getLocalPart().toLowerCase();
					if (startElementName == ITEM){
						message = new FeedMessage();
						isWithinTitle = true;
					}
					if (isWithinTitle){
						switch (startElementName){
							case DESCRIPTION:
								message.setDescription(reader.getElementText());
								break;
							case LINK:
								message.setLink(reader.getElementText());
								break;
							case TITLE:
								message.setTitle(reader.getElementText());
								break;
							case GUID:
								message.setGUID(reader.getElementText());
								break;
							case PUBDATE:
								message.setPubDate(reader.getElementText());
								break;
					}
					// System.out.println(startElement.getName());
					}
				}
				if (event.isEndElement()){
					EndElement endElement = event.asEndElement();
					String endElementName = endElement.getName().getLocalPart()
						.toLowerCase();
					if (endElementName == ITEM){
						feed.addMessage(message);
						isWithinTitle = false;
					}

				}
			}
			// for (FeedMessage m : feed.getMessages()){
			// 	System.out.printf("Title:\n %s\n", m.getTitle());
			// 	System.out.printf("PubDate:\n %s\n", m.getPubDate());
			// 	System.out.printf("Description:\n %s\n",m.getDescription());
			// 	System.out.printf("Link:\n %s\n", m.getLink());
			// 	System.out.printf("GUID:\n %s\n", m.getGUID());
			// }
		}
		catch(XMLStreamException e){
			log.log(Level.WARNING, "Can't read RSS XML. ", e);
			// System.out.println(e);
		}

		return feed;
	}
}
