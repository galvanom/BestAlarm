package server.rssparser;

public class FeedMessage{
	private String title = "";
	private String link = "";
	private String description = "";
	private String pubDate = "";
	private String guid = "";

	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = removeExtraSymbols(title);
	}
	public String getLink(){
		return this.link;
	}
	public void setLink(String link){
		this.link = link;
	}
	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = removeExtraSymbols(description);
	}
	public String getPubDate(){
		return this.pubDate;
	}
	public void setPubDate(String pubDate){
		this.pubDate = pubDate;
	}
	public String getGUID(){
		return this.guid;
	}
	public void setGUID(String guid){
		this.guid = guid;
	}
	String removeExtraSymbols(String string){
		StringBuilder sb = new StringBuilder(string);
		int i,j;
		//Delete &...; sequences
		for (i = 0; i < sb.length(); i++){
			if (sb.charAt(i) == '&'){
				for (j = i; sb.charAt(j) != ';';){
					j++;
				}
				sb.delete(i,j+1);
			}
		}

		return sb.toString();
	}
	@Override
	public int hashCode(){
		int result;
		result = title.hashCode() + link.hashCode() + pubDate.hashCode() + description.hashCode();
		return result;
	}
	@Override
	public boolean equals(Object obj){
		if (this == obj){
			return true;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		if (obj == null){
			return false;
		}
		FeedMessage fm = (FeedMessage)obj;

		if (this.hashCode() == fm.hashCode()){
			return true;
		}
		return false;
	}
}
