package server.rssparser;

import java.util.ArrayList;

public class Feed{
	private String encoding;
	private ArrayList<FeedMessage> messages;

	Feed(){
		messages = new ArrayList<FeedMessage>();
	}
	public void addMessage(FeedMessage message){
		messages.add(message);
	}
	public ArrayList<FeedMessage> getMessages(){
		return this.messages;
	}
	public  boolean isEmpty(){
		return messages.isEmpty();
	}
}
