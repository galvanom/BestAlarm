package server.weather.openweather;

import server.Settings;
import server.weather.Weather;

import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.lang.Math;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OpenWeather implements Weather{
	private static Logger log = Logger.getLogger(OpenWeather.class.getName());

	private class Forecast{
		protected int cityId = -1;
		protected String description;
		protected int currentTemp;
		protected int humidity;
		protected int windSpeed;
		public String toString(){
			return description + "\n" +
			String.valueOf(cityId) + "\n" +
			String.valueOf(currentTemp) + "\n" +
			String.valueOf(humidity) + "\n" +
			String.valueOf(windSpeed);

		}
	}
	private static Settings settings = Settings.getInstance();
	private static final String OWDomenBody = "http://api.openweathermap.org/data/2.5/weather?lang=ru&id=";
	private static final String OWAPIKey = settings.getValue("owkey");
	double latitude;
	double longtitude;
	public double getLongtitude(){
		return longtitude;
	}
	public double getLatitude(){
		return latitude;
	}
	//TODO: Do smth with type casting
	private Forecast parseForecast(String jsonForecast){
		Forecast forecast = new Forecast();
		try{
			JSONParser parser = new JSONParser();
			JSONObject jsObj = (JSONObject) parser.parse(jsonForecast);
			JSONObject weatherObj = (JSONObject) ((JSONArray) jsObj.get("weather")).get(0);
			forecast.description = (String) weatherObj.get("description");
			JSONObject mainObj = (JSONObject) jsObj.get("main");
			forecast.currentTemp = (int)((Double) mainObj.get("temp") - 273.15);
			forecast.humidity = ((Long) mainObj.get("humidity")).intValue();
			JSONObject windObj = (JSONObject) jsObj.get("wind");
			if (windObj.get("speed") instanceof Long){
				forecast.windSpeed = ((Long) windObj.get("speed")).intValue();
			}
			else{
				forecast.windSpeed = ((Double) windObj.get("speed")).intValue();
			}

			forecast.cityId = ((Long)jsObj.get("id")).intValue();
		}
		catch (ParseException e){
			log.log(Level.WARNING, "Forecast parser error.", e);
			// System.out.println(e);
		}
		return forecast;
	}

	public static void main(String args[]){
		// String jsonForecast = "{\"coord\":{\"lon\":29.97,\"lat\":59.87},\"weather\":[{\"id\":600,\"main\":\"Snow\",\"description\":\"Р Р…Р ВµР В±Р С•Р В»РЎРЉРЎв‚¬Р С•Р в„– РЎРѓР Р…Р ВµР С–Р С•Р С—Р В°Р Т‘\",\"icon\":\"13d\"}],\"base\":\"stations\",\"main\":{\"temp\":272.15,\"pressure\":1012,\"humidity\":80,\"temp_min\":272.15,\"temp_max\":272.15},\"visibility\":7000,\"wind\":{\"speed\":5,\"deg\":360},\"clouds\":{\"all\":75},\"dt\":1478262600,\"sys\":{\"type\":1,\"id\":7267,\"message\":0.1543,\"country\":\"RU\",\"sunrise\":1478237489,\"sunset\":1478267698},\"id\":507887,\"name\":\"Poeziya\",\"cod\":200}";

		String message = new OpenWeather(37.617300, 55.755826).getMessage();
		System.out.println(message);
		// System.out.println(new OpenWeather(150.805294,59.557749).getEnd(23));

	}
	public OpenWeather(double longtitude, double latitude){
		this.latitude = latitude;
		this.longtitude = longtitude;
		getCities();
	}
	public static void getCities(){
		if (!OpenWeatherCities.isCitiesLoaded()){
			Settings settings = Settings.getInstance();
			String citiesFile = settings.getValue("owcities");
			if (!OpenWeatherCities.getCitiesFromFile(citiesFile)){
				System.out.println("Cities load failed\n");
			}
		}
	}
	//		Example:"Р СџР С•Р С–Р С•Р Т‘Р В°. Р С›Р В±Р В»Р В°РЎвЂЎР Р…Р С•. Р СћР ВµР С�Р С—Р ВµРЎР‚Р В°РЎвЂљРЎС“РЎР‚Р В° Р Р†Р С•Р В·Р Т‘РЎС“РЎвЂ¦Р В° Р С�Р С‘Р Р…РЎС“РЎРѓ 23 Р С–РЎР‚Р В°Р Т‘РЎС“РЎРѓР В° РЎвЂ Р ВµР В»РЎРЉРЎРѓР С‘РЎРЏ.
	//				Р вЂ™Р В»Р В°Р В¶Р Р…Р С•РЎРѓРЎвЂљРЎРЉ 80 Р С—РЎР‚Р С•РЎвЂ Р ВµР Р…РЎвЂљР С•Р Р†. Р РЋР С”Р С•РЎР‚Р С•РЎРѓРЎвЂљРЎРЉ Р Р†Р ВµРЎвЂљРЎР‚Р В° 5 Р С�Р ВµРЎвЂљРЎР‚Р С•Р Р† Р Р† РЎРѓР ВµР С”РЎС“Р Р…Р Т‘РЎС“."
	public String getMessage(){
		StringBuffer message = new StringBuffer();
		Forecast forecast = getWeather();
		if (forecast != null){
			// System.out.println(forecast.toString());
			// Sometimes OW service returns english description instead of russian
			// for example:
			// String message = new OpenWeather(37.617300, 55.755826).getMessage();
			// message.append(forecast.description);
			// message.append(". ");
			message.append("Температура воздуха ");
			if (forecast.currentTemp < 0){
				message.append("минус ");
			}
			message.append(Math.abs(forecast.currentTemp));
			message.append(" градус");
			message.append(getEnd(forecast.currentTemp));
			message.append(" цельсия. Влажность ");
			message.append(forecast.humidity);
			message.append(" процент");
			message.append(getEnd(forecast.humidity));
			message.append(". Скорость ветра ");
			message.append(forecast.windSpeed);
			message.append(" метр");
			message.append(getEnd(forecast.windSpeed));
			message.append(" в секунду.");
		}
		return message.toString();

	}
	private String getEnd(int number){
		String end;
		switch (Math.abs(number % 10)){
			case 1:
				end = "";
				break;
			case 2:
			case 3:
			case 4:
				end = "а";
				break;
			default:
				end = "ов";
		}
		// Special case for *11, *12 .. 
		switch (Math.abs(number % 100)){
			case 11:
			case 12:
			case 13:
			case 14:
				end = "ов";
				break;
		}
		return end;
	}
	public Forecast getWeather(){
		String jsonForecast = getWeatherJSON();

		if (jsonForecast.length() == 0){
			return null;
		}
		Forecast forecast = parseForecast(jsonForecast);
		if (forecast.cityId < 0) {
			return null;
		}
		else{
			return forecast;
		}
	}
	public String getWeatherJSON(){
		final int ATTEMPTS = 3;
		int cityId = getCityId();
		String fullURL = OWDomenBody + Integer.toString(cityId) + "&appid="+OWAPIKey;
		StringBuffer response = new StringBuffer();
		// System.out.println(fullURL);
		try{
			int code = 0;
			
			URL url = new URL(fullURL);
			HttpURLConnection connection = null;
			for (int i = 0; code != 200 && i < ATTEMPTS; i++){
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
				connection.connect();
				code = connection.getResponseCode();
			}
			//TODO: Throw exception}

			if (code == 200){

				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				while((line = in.readLine()) != null){
					response.append(line);
				}
				in.close();
			}
			else{
				log.log(Level.WARNING, "OpenWeather answer code differs from 200. Code = "
					+ String.valueOf(code));
			}

			// System.out.println(response.toString());
		}
		catch(IOException e){
			log.log(Level.WARNING, "OpenWeather connection failed.", e);
			// System.out.println(e);
		}

		return response.toString();
	}
	public int getCityId(){
		return getCityIdByCoord(this.longtitude, this.latitude);
	}
	private int getCityIdByCoord(double longtitude, double latitude){
		OpenWeatherCities cities = new OpenWeatherCities();
		return (int) cities.getCityID(longtitude, latitude);
	}
}