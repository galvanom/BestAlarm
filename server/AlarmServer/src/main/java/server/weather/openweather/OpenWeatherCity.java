package server.weather.openweather;

public class OpenWeatherCity{
	long _id;
	String name;
	String country;
	double longtitude;
	double latitude;
	public OpenWeatherCity(long _id, String name, String country,
		double longtitude, double latitude){

		this._id = _id;
		this.name = name;
		this.country = country;
		this.longtitude = longtitude;
		this.latitude = latitude;
	}
	public long getID(){
		return _id;
	}
	public String getName(){
		return name;
	}
	public String getCountry(){
		return country;
	}
	public double getLongtitude(){
		return longtitude;
	}
	public double getLatitude(){
		return latitude;
	}
	@Override
	public String toString(){
		return String.valueOf(_id) + "\n" + name + "\n" + 
		country + "\n" + String.valueOf(longtitude) + 
		"\n" + String.valueOf(latitude);
	}

}