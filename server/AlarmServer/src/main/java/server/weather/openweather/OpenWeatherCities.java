package server.weather.openweather;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class OpenWeatherCities{
	private static ArrayList<OpenWeatherCity> cities = new ArrayList<OpenWeatherCity>();

	public long getCityID(double longtitude, double latitude){
		OpenWeatherCity owc = getCity(longtitude, latitude);

		if (owc != null){
			return owc.getID();
		}
		return -1;
	}
	private OpenWeatherCity getCity(double longtitude, double latitude){
		double distance = -1.0;
		double bestDistance = 40000000.0;
		final double THRESHOLD = 10.0;
		OpenWeatherCity bestCity = null;
		for (OpenWeatherCity city : OpenWeatherCities.cities){

			if (Math.abs(city.getLongtitude() - longtitude) > THRESHOLD){
				continue;
			}
			if (Math.abs(city.getLatitude() - latitude) > THRESHOLD){
				continue;
			}
			distance = getGPSDistance(city.getLongtitude(),
				city.getLatitude(), longtitude, latitude);
			if (distance < bestDistance){
				bestDistance = distance;
				bestCity = city;
			}

		}
		
		// System.out.println(bestDistance);
		return bestCity;
	}
	/*
	* Returns distance between two GPS points in meters
	* Formula: http://gis-lab.info/qa/great-circles.html
	* Haversine formula (adapter)
	*/
	private double getGPSDistance(double lon1, double lat1, double lon2, double lat2){
		final double EARTH_RADIUS = 6372795.0;
		double rlat1, rlon1, rlat2, rlon2;
		double cl1, cl2, sl1, sl2, delta, cdelta, sdelta;
		double x, y, ad, dist;

		rlat1 = Math.toRadians(lat1);
		rlat2 = Math.toRadians(lat2);
		rlon1 = Math.toRadians(lon1);
		rlon2 = Math.toRadians(lon2);

		cl1 = Math.cos(rlat1);
		cl2 = Math.cos(rlat2);
		sl1 = Math.sin(rlat1);
		sl2 = Math.sin(rlat2);
		delta = rlon2 - rlon1;
		cdelta = Math.cos(delta);
		sdelta = Math.sin(delta);

		y = Math.sqrt(Math.pow(cl2*sdelta,2) + Math.pow(cl1*sl2 - sl1*cl2*cdelta,2));
		x = sl1*sl2 + cl1*cl2*cdelta;
		ad = Math.atan2(y,x);
		dist = ad*EARTH_RADIUS;

		return dist;
	}

	private static OpenWeatherCity parseJSON(String js){
		OpenWeatherCity city = null;
		OpenWeatherCities cities = new OpenWeatherCities();
		long _id;
		String name;
		String country;
		double longtitude, latitude;
		try {

			JSONParser parser = new JSONParser();
			Object obj = parser.parse(js);
			JSONObject jsObj = (JSONObject) obj;
			_id = (Long) jsObj.get("_id");
			name = (String) jsObj.get("name");
			country = (String) jsObj.get("country");
			JSONObject coord = (JSONObject) jsObj.get("coord");
			if (coord.get("lon") instanceof Long){
				longtitude = ((Long) coord.get("lon")).doubleValue();
			}
			else{
				longtitude = (Double) coord.get("lon");
			}
			if (coord.get("lat") instanceof Long){
				latitude = ((Long) coord.get("lat")).doubleValue();
			}
			else{
				latitude = (Double) coord.get("lat");
			}
			city = new OpenWeatherCity(_id, name, country, longtitude, latitude);
			// System.out.println(city.toString());
			
		}
		catch(ParseException e){
			System.out.println(e);
		}
		return city;
	}
	public static boolean getCitiesFromFile(String fileName){
		FileInputStream fis;
		InputStreamReader isr;
		BufferedReader br;
		final int CAPACITY = 200000;
		ArrayList<OpenWeatherCity> cities = new ArrayList<OpenWeatherCity>();
		OpenWeatherCity city;

		try{
			fis = new FileInputStream(fileName);
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);
			String  line = br.readLine();
			
			while (line != null){
				// System.out.println(line);
				city = parseJSON(line);
				// System.out.println(city.getID());
				cities.add(city);
				line = br.readLine();
			}
		}
		catch (FileNotFoundException e){
			System.out.println(e);
		}
		catch (IOException e){
			System.out.println(e);
		}
		if (cities.size() > 0){
			// System.out.println(cities.size());
			OpenWeatherCities.cities = cities;
			return true;
		}
		return false;
	}
	public static boolean isCitiesLoaded(){
		return cities.size() > 0;
	}
	
}