package server;

import server.rssparser.FeedMessage;
import server.rssparser.Feed;
import server.rssparser.RSSReader;
import server.texttospeech.ivona.TTSIvona;
import server.texttospeech.ivona.TTSIvona.VoiceName;
import server.weather.openweather.OpenWeather;
import server.weather.openweather.OpenWeatherCities;
import server.weather.openweather.OpenWeatherCity;

import java.io.IOException;
import java.util.LinkedList;
import java.util.ArrayList;
import java.io.File;
import java.util.logging.*;
public class FileManager{
	private String path;
	private ArrayList<News> newsList;
	private ArrayList<GPSCoord> gpsList;

	private static Logger log = Logger.getLogger(FileManager.class.getName());
	private final static Settings settings = Settings.getInstance();

	// Path without "\\" at the end
	public FileManager(String path){
		this.path = path;
		newsList = new ArrayList<News>();
		gpsList = new ArrayList<GPSCoord>();

		// Load file with cities info to memory
		log.info("Loading OpenWeather cities...");
		OpenWeather.getCities();
		log.info("OpenWeather cities has been loaded");
	}

	public enum NewsProvider{
		Lenta, Yandex;

		public String getRssUrl(){
			String url = "";
			switch(this){
				case Lenta:
					url = "https://lenta.ru/rss/last24";
					break;
				case Yandex:
					url = "https://news.yandex.ru/index.rss";
					break;
				default:
			}
			return url;
		}
		public String toString(){
			String str = "";
			switch(this){
				case Lenta:
					str = "lenta";
					break;
				case Yandex:
					str = "yandex";
					break;
				default:
			}
			return str;
		}
		public static NewsProvider fromString(String str){
			NewsProvider provider = null;
			switch(str){
				case "lenta":
					provider = Lenta;
					break;
				case "yandex":
					provider = Yandex;
					break;
				default:
			}
			return provider;

		}
	}
	// Describies weather file
	private class GPSCoord{
		public double longtitude, latitude;
		public long cityId;
		public String filePath;
		public VoiceName voiceName;
		public long fileTimestamp;

		GPSCoord(double longtitude, double latitude){
			this.longtitude = longtitude;
			this.latitude = latitude;
		}
		GPSCoord(double longtitude, double latitude, long cityId){
			this.longtitude = longtitude;
			this.latitude = latitude;
			this.cityId = cityId;
		}

	}
	// Describes news file
	private class News{
		public NewsProvider provider;
		public VoiceName voiceName;
		public long fileTimestamp;
		public String filePath;

		public boolean isIt(News other){
			if (other.provider == this.provider &&
				other.voiceName == this.voiceName){
				return true;
			}
			return false;
		}
	}
	// Returns voiced news file name or an empty string
	public String getNewsFileName(final NewsProvider provider, final VoiceName voiceName){
		// Amount of milliseconds before news are got old
		final long THRESSHOLD = 3600000;
		News newsRequest = new News();
		newsRequest.provider = provider;
		newsRequest.voiceName = voiceName;
		// Search similar newsfile in existing files. News file not older then THRESSHOLD
		synchronized(newsList){
			for (News news : newsList){
				if (news.isIt(newsRequest)){
					if ((System.currentTimeMillis() - news.fileTimestamp) < THRESSHOLD){
						return news.filePath;
					}

				}
			}
		}
		// If there is no file, try to get it within separated thread
		// and return empty String
		new Thread(new Runnable(){
			public void run(){
				getNewsBigFile(provider, voiceName);
			}
		}).start();

		return "";
	}
	// Downloads latest news from news provider and voices them.
	// Saves file with specified name in path
	private void getNewsBigFile(NewsProvider provider, VoiceName voiceName){
		News currentNews = new News();
		// Get rss feed messages
		RSSReader rss = new RSSReader();
		Feed feed = rss.readFromURL(provider.getRssUrl());
		// Wrap number of messages in SSML
		String text = getSSMLNewsText(feed, 5);
		// Form full file name
		StringBuilder fullPath = new StringBuilder()
								.append("news_")
								.append(provider.toString())
								.append("_")
								.append(voiceName.toString().toLowerCase())
								.append(".mp3");
		// Voice text with Ivona
		boolean written = new TTSIvona().getSpeech(text, voiceName, path+"/"+fullPath.toString());
		if (written){
		    // Make news object
		    currentNews.provider = provider;
		    currentNews.voiceName = voiceName;
		    currentNews.fileTimestamp = System.currentTimeMillis();
		    currentNews.filePath = fullPath.toString();

		    // Search  similar object in existing objects
		    // If found the same object, insert new one on it's place
		    // else add new one to the collection's tail
		    int i = 0;
		    boolean isAlreadyExists = false;

		    synchronized (newsList){
			    for (News news : newsList){
				    if (news.isIt(currentNews)){
					    isAlreadyExists = true;
					    newsList.set(i, currentNews);
					    break;
				    }
				    i++;
			    }
			    if (!isAlreadyExists){
				    newsList.add(currentNews);
				}
		    }

		}
		else{
			log.info("File " + fullPath + " has not been written");
		}

	}
	// Wrap number of messages to SSML
	private String getSSMLNewsText(Feed feed, int numberOfMessagesToAdd){
		ArrayList<FeedMessage> feedMessages = feed.getMessages();
		StringBuilder ssmlText = new StringBuilder();
		String title = "Новости ";
		// Add ssml header with russian language option
		ssmlText.append("<?xml version=\"1.0\"?><speak version=\"1.0\" xml:lang=\"ru\">");
		ssmlText.append(title);
		ssmlText.append("<break time='1000ms' /> ");

		for (int i = 0; i < numberOfMessagesToAdd && i < feedMessages.size(); i++){
			ssmlText.append(feedMessages.get(i).getTitle());
			ssmlText.append(" <break time='1000ms' />");
		}
		// Add closing tag
		ssmlText.append("</speak>");

		return ssmlText.toString();
	}
	private String getSSMLWeatherText(String text){
		StringBuilder ssmlText = new StringBuilder();
		String title = "Погода ";
		ssmlText.append("<?xml version=\"1.0\"?><speak version=\"1.0\" xml:lang=\"ru\"><s>");
		ssmlText.append(title);
		ssmlText.append("<break time='1000ms' /> ");
		ssmlText.append(text);
		ssmlText.append("</s></speak>");

		return ssmlText.toString();
	}

	public String getWeatherFileName(double longtitude, double latitude, final VoiceName voiceName){
		final long THRESSHOLD = 7200000;
		final OpenWeather openWeather = new OpenWeather(longtitude,latitude);
		long cityId = openWeather.getCityId();
		final GPSCoord currentGPSCoord = new GPSCoord(longtitude, latitude, cityId);
		currentGPSCoord.voiceName = voiceName;

		synchronized (gpsList){
			for (GPSCoord gpsCoord : gpsList){
				if (gpsCoord.cityId == currentGPSCoord.cityId &&
					gpsCoord.voiceName == currentGPSCoord.voiceName &&
					(System.currentTimeMillis() - THRESSHOLD) < gpsCoord.fileTimestamp){
					return gpsCoord.filePath;
				}
			}
			new Thread(new Runnable(){
				public void run(){
					getWeatherFile(openWeather, currentGPSCoord, voiceName);
				}
			}).start();
		}

		return "";

	}
	private void getWeatherFile(OpenWeather openWeather, GPSCoord currentGPS, VoiceName voiceName){
		String openWeatherMessage = openWeather.getMessage();
		if (openWeatherMessage.equals("")){
			log.log(Level.WARNING, "OpenWeather message is empty");
		}
		else{
			String text = getSSMLWeatherText(openWeatherMessage);
			StringBuilder fullPath = new StringBuilder()
									.append("weather_")
									.append(currentGPS.cityId)
									.append("_")
									.append(voiceName.toString().toLowerCase())
									.append(".mp3");
			boolean written = new TTSIvona().getSpeech(text, voiceName, path+"/"+fullPath.toString());
			// System.out.println(text);


			if (written){
				currentGPS.fileTimestamp = System.currentTimeMillis();
				currentGPS.filePath = fullPath.toString();

				int i = 0;
				boolean isAlreadyExists = false;

				synchronized (gpsList){
					for (GPSCoord gps : gpsList){
						if (gps.cityId == currentGPS.cityId &&
							gps.voiceName == currentGPS.voiceName){
							isAlreadyExists = true;
							gpsList.set(i, currentGPS);
							break;
						}
						i++;
					}
					if (!isAlreadyExists){
						gpsList.add(currentGPS);
					}

				}
			}
			else{
				log.info("File " + fullPath + " has not been written");
			}
		}

	}

}