<?php
	include_once "client.php";
	$path = "./files";
	$file = "";
	if (array_key_exists("type", $_GET)){
		if ($_GET["type"] == "news"){
			//$file = "files/news/bigfile.mp3";
			if (array_key_exists("provider", $_GET) and  array_key_exists("voice", $_GET)){
			    // Form request
			    $request["provider"] = $_GET["provider"];
			    $request["type"] = "news";
			    $request["voice"] = $_GET["voice"];
			    // Get answer from server
			    // If there is no such file
			    // server returns an empty string
			    // also server returns path to the file
			    
			    $file = trim(get_file_path(json_encode($request)));
			    
			    if (!empty($file)){
				$file = $path . "/" . $file;
			    }
			}
			
		}
		if ($_GET["type"] == "gps"){
		    if (array_key_exists("long", $_GET) and array_key_exists("lat", $_GET) and
			array_key_exists("voice", $_GET)){
			
			$request["type"] = "gps";
			$request["long"] = $_GET["long"];
			$request["lat"] = $_GET["lat"];
			$request["voice"] = $_GET["voice"];
			
			$file = trim(get_file_path(json_encode($request)));
			
			if (!empty($file)){
			    $file = $path . "/" . $file;
			}	
		    
		    }
		
		}
		if (file_exists($file)){
			echo "File exists";
			header('Content-Description: File Transfer');
	    		header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($file).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
		}
	}
	
?>
