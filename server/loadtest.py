import urllib
import random
import string
import os
import threading
from time import sleep

def download(address, parameters):
	params = urllib.urlencode(parameters)
	url = address + params
	connection = urllib.urlopen(url)
	connection_info = connection.info()
	if connection_info.type == 'audio/mpeg':
		while True:
			file_name = generate_filename(6)
			if os.path.isfile(file_name) == False:
				break
		f = open(file_name,'w')
		file_length = connection_info.get('Content-Length')
		f.write(connection.read())
		if file_length:
			if file_length == os.stat(file_name).st_size:
				print file_name + " from " + url + " downloaded sucessfully"
			else:
				print file_name + " from " + url + " downloaded partly"		
				print "Expected length: " + file_length
				print "Real length: " +  str (os.stat(file_name).st_size)	
		f.close()
	else:
		print "File not found"
	connection.close()

def generate_filename(N):
	file_name = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(N)) + '.mp3'
	return "loadtest//"+file_name

address = "http://93.95.97.58:8080/alarmserver/MainServlet?"
parameters1 = {'type':'news', 'provider':'yandex', 'voice':'tatyana'}
parameters2 = {'type':'gps', 'long': 30.3, 'lat': 59.8, 'voice':'tatyana'}
# download(address, parameters)
for i in range(1000):
	threading.Thread(target=download, args=(address,parameters1)).start()
	threading.Thread(target=download, args=(address,parameters2)).start()
	sleep(0.001);
#threads logic
