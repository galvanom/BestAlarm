package com.alarm.androidalarm;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
public class CancelAlarmActivity extends Activity {
    Alarms alarms = Alarms.getInstance();
    MediaPlayer backgroundPlayer;
    MediaPlayer downloadedPlayer;
    MediaPlayer melodyPlayer;
    AlarmVolume alarmVolume;
    private final long melodyDuration = 60000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_alarm);
        // Set flags for activity start while screen locked
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        alarms.setAll();
        final LinkedList<String> filesList = new LinkedList<>();
        alarmVolume = new AlarmVolume();

        //Set default melody
        int melodyResourceID = R.raw.hours;
        //Set default volume level
        int volumeLevel = 30;
        // Get files from intent
        Intent intent  = getIntent();
        if (intent == null){
            Log.d("DEBUG", "CancelActivity: can't get intent");
        }
        else {
            if (intent.hasExtra("Melody")){
                melodyResourceID = intent.getIntExtra("Melody", melodyResourceID);
            }
            if (intent.hasExtra("Volume")){
                volumeLevel = intent.getIntExtra("Volume", volumeLevel);
            }
            if (intent.hasExtra(AlarmItem.TTSItemsNames.YA_NEWS.toString())){
                String newsFileName =
                        intent.getStringExtra(AlarmItem.TTSItemsNames.YA_NEWS.toString());
                filesList.add(newsFileName);
            }
            if (intent.hasExtra(AlarmItem.TTSItemsNames.OPENWEATHER.toString())){
                String weatherFileName =
                        intent.getStringExtra(AlarmItem.TTSItemsNames.OPENWEATHER.toString());
                filesList.add(weatherFileName);
            }
            Log.d("DEBUG", "Audio files names:");
            for  (String file : filesList) {
                Log.d("DEBUG", file);
            }
        }

        // Set players settings
        alarmVolume.setUserVolume(volumeLevel);
        backgroundPlayer = new MediaPlayer();
        backgroundPlayer.setLooping(true);
        backgroundPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        final Uri backgroundUri = Uri.parse("android.resource://com.alarm.androidalarm/" + R.raw.bn_cut);
        try {
            backgroundPlayer.setDataSource(getApplicationContext(), backgroundUri);
            backgroundPlayer.prepare();
        }
        catch(IOException e){
            Log.d("DEBUG", "Can't load and prepare background file");
            backgroundPlayer = null;
        }

        downloadedPlayer = new MediaPlayer();
        downloadedPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        downloadedPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                    if (backgroundPlayer != null) {
                        backgroundPlayer.start();
                    }
                    mp.start();
            }
        });
        downloadedPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                if (!filesList.isEmpty()) {
                    playNextFile(filesList);
                }
                else{
                    closePlayer(backgroundPlayer);
                }
            }
        });

        // Cancel button behavior
        final Button stopButton = (Button) findViewById(R.id.button4);

        if (filesList.isEmpty()){
            stopButton.setText(R.string.cancel_button_stop);
        }
        else{
            stopButton.setText(R.string.cancel_button_next);
        }
        View.OnClickListener stopButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (melodyPlayer != null){
                        closePlayer(melodyPlayer);
                        melodyPlayer = null;
                        if (filesList.isEmpty()){
                            closeActivity();
                        }
                        else {
                            playNextFile(filesList);
                            stopButton.setText(R.string.cancel_button_stop);
                        }

                    } else {
                        clearAllMediaPlayers();
                        closeActivity();
                    }
                }
                catch (IllegalStateException e){
                    Log.d("DEBUG", "Illegal state " + e.toString());
                    closeActivity();
                }
            }
        };
        stopButton.setOnClickListener(stopButtonListener);

        // Start playing files
        playMelody(melodyResourceID);
    }
    private void closePlayer(MediaPlayer mp){
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
            mp.release();
        }
    }
    private void playMelody(int resId){
        melodyPlayer = new MediaPlayer();
        melodyPlayer.setLooping(true);
        melodyPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        Uri resourceUri =  Uri.parse("android.resource://com.alarm.androidalarm/" + resId);
        try {
            melodyPlayer.setDataSource(getApplicationContext(),resourceUri);
            melodyPlayer.prepareAsync();
        }
        catch(IOException e){
            Log.d("DEBUG", "Can't load and prepare melody file");
            return;
        }
        melodyPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                melodyPlayer.start();
            }
        });

        // Melody autostop
        final Handler melodyHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
            super.handleMessage(msg);
            closePlayer(melodyPlayer);
            melodyPlayer = null;
            }
        };
        Timer melodyTimer = new Timer();
        melodyTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                melodyHandler.sendEmptyMessage(0);
                try {
                    if (downloadedPlayer != null && !downloadedPlayer.isPlaying()) {
                        closeActivity();
                    }
                }
                //TODO: Fix illegal state exception
                catch (IllegalStateException e){
                    Log.d("DEBUG","DownloadedPlayer illegal state " +  e);
                    closeActivity();
                }
            }
        }, melodyDuration);
    }
    private void playNextFile(LinkedList<String> filesList){
        if(!filesList.isEmpty()) {
            try {
                String fileName = filesList.poll();
                Log.d("DEBUG", "Pick file " + fileName);
                File audioFile = new File(getFilesDir(), fileName);
                // if file exists and its not older than 10000 seconds
                // and if it bigger then 1KB
                if (audioFile.exists() &&
                        (System.currentTimeMillis() - audioFile.lastModified() < 10000 * 1000) &&
                        audioFile.length() > 1024) {
                    Log.d("DEBUG", "File exists, time is actual " + audioFile.getAbsolutePath());
                    FileInputStream fis = new FileInputStream(audioFile);

                    downloadedPlayer.setDataSource(fis.getFD());
                    downloadedPlayer.prepareAsync();
                }
                else if (filesList.isEmpty()){
                    closePlayer(backgroundPlayer);
                }

            } catch (IOException e) {
                Log.d("DEBUG", e.toString());
                this.finish();
            }
            catch (IllegalStateException e){
                Log.d("DEBUG", e.toString());
            }

        }
        else{
            clearAllMediaPlayers();
            closeActivity();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearAllMediaPlayers();
    }
    private void closeActivity(){
        if(android.os.Build.VERSION.SDK_INT >= 21) {
            finishAndRemoveTask();
        }
        else {
            finish();
        }
    }
    private void clearAllMediaPlayers(){
        try {
            closePlayer(melodyPlayer);
            closePlayer(downloadedPlayer);
            closePlayer(backgroundPlayer);
            melodyPlayer = downloadedPlayer = backgroundPlayer = null;
            alarmVolume.setBack();
        }
        catch (IllegalStateException e){
            Log.d("DEBUG", "Illegal state " + e.toString());
        }

    }
    private class AlarmVolume{
        private int userVolume;
        private int maxVolume;
        AudioManager mAudioManager;
        AlarmVolume(){
            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            userVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
            maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM);
        }
        public void setMax(){
            mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, maxVolume, 0);
        }
        public void setBack(){
            mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, userVolume, 0);
        }
        public void setUserVolume(int volume){
            double dVolume =  (double)volume / 100 * maxVolume;
            Log.d("DEBUG", "Volume level: " + String.valueOf(dVolume));
            mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, dVolume >= 1.0 ? (int)(dVolume) : 1, 0);
        }
    }
}
