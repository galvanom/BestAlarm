package com.alarm.androidalarm;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
public class AlarmApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        Log.d("Application","Hello");
        Alarms.init(getApplicationContext());
        LocationProvider.init(getApplicationContext());

        // Show notification
        Alarms.getInstance().changeAlarmNotification();
    }
}
