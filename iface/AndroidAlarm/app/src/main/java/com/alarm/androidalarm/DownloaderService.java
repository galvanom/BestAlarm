package com.alarm.androidalarm;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

public class DownloaderService extends Service {
    private final int NO_EXTRAS = -1;
    Alarms alarms;
    Context context = this;

    // Class for download audio files from remote server
    class  DownloadAlarmFiles implements Runnable {
        private AlarmItem alarmItem;
        private int id;
        private final static int NEXT_TRY_TIME = 30_000;
        DownloadAlarmFiles(AlarmItem alarmItem, int id){
            this.alarmItem = alarmItem;
            this.id = id;
        }
        @Override
        public void run() {
            final String HTTP_ADDRESS = "http://galvanom.com:8080/alarmserver/MainServlet";
            AudioDownloader audioDownloader = new AudioDownloader();
            boolean downloadResult = true;

            Log.d("DEBUG", String.valueOf(alarmItem.getHour()) + ":"
                    + String.valueOf(alarmItem.getMinute()) + " Download started");

            for (AlarmItem.TTSItemsNames item : AlarmItem.TTSItemsNames.values()){
                if (!alarmItem.isItemChecked(item)){
                    continue;
                }
                String fileName = alarmItem.getFileName(item);
                Log.d("DEBUG", "Filename: " + fileName);
                String urlParameters = alarmItem.getURLParameters(item);
                Log.d("DEBUG", "url: " + urlParameters);

                if (fileName.isEmpty() || urlParameters.isEmpty()){
                    Log.d("DEBUG", "File name or url parameters is empty");
                    downloadResult = false;
                    continue;
                }

                if (actualFilePresent(fileName, item.getActualTime())){
                    Log.d("DEBUG", "Actual file already exists");
                    continue;
                }
                try {
                   if (audioDownloader.getFileAndSave(context, HTTP_ADDRESS + urlParameters, fileName)){
                        Log.d("DEBUG", "File download succeded!");
                    }
                    else{
                        Log.d("DEBUG", "File download failed!");
                       downloadResult = false;
                    }
                }
                catch(IOException e){
                    Log.d("DEBUG", e.toString());
                    downloadResult = false;
                }

            }
            if (!downloadResult){
                long nextDownloadTime = System.currentTimeMillis() + NEXT_TRY_TIME;
                long alarmTime = alarmItem.getRunTimeMillis();
                if (nextDownloadTime < alarmTime) {
                    alarmItem.downloadAt(getApplicationContext(), nextDownloadTime, id);
                }
            }
        }
    }
    boolean actualFilePresent(String fileName, long actualTime){
        File file = new File(context.getFilesDir(), fileName);
        if (file.exists() && file.length() != 0){
          if ((System.currentTimeMillis() - file.lastModified()) > actualTime){
              return false;
          }
        }
        else {
            return false;
        }
        return true;
    }
    public DownloaderService() {
        alarms = Alarms.getInstance();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null){
            if (intent.hasExtra("alarmHour")
                    && intent.hasExtra("alarmMinute")
                    && intent.hasExtra("pendingIntentId")) {
                int hour = intent.getIntExtra("alarmHour", NO_EXTRAS);
                int minute = intent.getIntExtra("alarmMinute", NO_EXTRAS);
                int id = intent.getIntExtra("pendingIntentId", NO_EXTRAS);

                // Find this item
                AlarmItem alarmItem = null;
                for (AlarmItem item : alarms.getAlarmItems()) {
                    // Item should have the same time parameters
                    if (item.getHour() == hour
                            && item.getMinute() == minute
                            && item.isTodayAlarmDay()) {
                        // and it is time to download item's files
                        if (System.currentTimeMillis() > item.getDownloadStartTime()) {
                            alarmItem = item;
                            break;
                        }
                    }
                }
                if (alarmItem != null) {
                    DownloadAlarmFiles runDownloader = new DownloadAlarmFiles(alarmItem, id);
                    Thread networkDownloader = new Thread(runDownloader);
                    networkDownloader.start();
                } else {
                    Log.d("DEBUG", "Can't find alarm item that equals this intent");
                }
            }
            else{
                Log.d("DEBUG", "No extras found for download");
            }
        }
        else{
            Log.d("DEBUG", "Download service intent == null");
        }

        return 0;
    }

}
