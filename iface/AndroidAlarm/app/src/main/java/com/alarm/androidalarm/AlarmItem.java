package com.alarm.androidalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.util.Log;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class AlarmItem {
    public synchronized boolean isChecked() {
        return isChecked;
    }

    public synchronized void setChecked(boolean checked) {
        isChecked = checked;
    }

    private boolean isChecked;

    public synchronized int getHour() {
        return hour;
    }

    public synchronized void setHour(int hour) {
        this.hour = hour;
    }

    private int hour;

    public synchronized int getMinute() {
        return minute;
    }

    public synchronized void setMinute(int minute) {
        this.minute = minute;
    }

    private int minute;
    private boolean[] daysOfWeek;

    public AlarmItem(boolean isChecked, int hour, int minute){
        this.isChecked = isChecked;
        this.hour = hour;
        this.minute = minute;

        this.daysOfWeek = new boolean[7];
        for (int i = 0; i < this.daysOfWeek.length; i++){
            this.daysOfWeek[i] = true;
        }
        // Assign default melody
        this.melody = Melodies.Hours;

        //Set default volume level
        volumeLevel = 30;

        ttsItems = new HashMap<TTSItemsNames, Boolean>();
        ttsItems.put(TTSItemsNames.YA_NEWS, true);
        ttsItems.put(TTSItemsNames.OPENWEATHER, true);

        ttsVoice = TTSVoices.IVONA_TATYANA;

    }

    public boolean[] getDaysOfWeek() {
        return daysOfWeek;
    }
    public void setDaysOfWeek(int dayNumber, boolean value){
            daysOfWeek[dayNumber] = value;
    }
    public enum Melodies{
        GoodMorning, Hours, WakeUp;
        public int getResId(){
            int resId = R.raw.hours;
            switch (this){
                case GoodMorning:
                    resId = R.raw.good_morning;
                    break;
                case Hours:
                    resId = R.raw.hours;
                    break;
                case WakeUp:
                    resId = R.raw.wakeup;
                    break;
            }
            return resId;
        }
    }

    public int getVolumeLevel() {
        return volumeLevel;
    }

    public void setVolumeLevel(int volumeLevel) {
        this.volumeLevel = volumeLevel;
    }

    private int volumeLevel;

    private Melodies melody;
    public void setMelody(Melodies melody){
        this.melody = melody;
    }
    public Melodies getMelody(){
        return this.melody;
    }

    private ArrayList<TTSItemsNames> getAllEnabledTTSItems(){
        ArrayList<TTSItemsNames> items = new ArrayList<>();
        for (HashMap.Entry entry : ttsItems.entrySet()){
            if ((boolean)entry.getValue()){
                items.add((TTSItemsNames)entry.getKey());
            }
        }
        return  items;
    }
    public enum TTSItemsNames{
        YA_NEWS, OPENWEATHER;
        public String getStringName(){
            String stringName = "";
            switch (this){
                case YA_NEWS:
                    stringName = "Последние новости";
                    break;
                case OPENWEATHER:
                    stringName = "Погода";
                    break;
                default:
            }
            return stringName;
        }
        public String getItemType(){
            String stringName = "";
            switch (this){
                case YA_NEWS:
                    stringName = "news";
                    break;
                case OPENWEATHER:
                    stringName = "gps";
                    break;
                default:
            }
            return stringName;
        }
        public String getNewsProviderName(){
            String stringName = "";
            switch (this){
                case YA_NEWS:
                    stringName = "yandex";
                    break;
                default:
            }
            return stringName;
        }
        public long getActualTime(){
            long time = 0;
            switch (this){
                case YA_NEWS:
                    time = 3600 * 1000;
                    break;
                case OPENWEATHER:
                    time = 7200 * 1000;
                    break;
                default:
            }
            return time;
        }

    }
    private HashMap<TTSItemsNames, Boolean> ttsItems;
    public void setTTSItem(TTSItemsNames name, Boolean value){
        ttsItems.put(name,value);
    }
    public Boolean getTTSItem(TTSItemsNames name){
        return ttsItems.get(name);
    }
    public boolean isItemChecked(TTSItemsNames item){
        return ttsItems.get(item);
    }
    public enum TTSVoices{
        IVONA_TATYANA, IVONA_MAXIM;
        public String getStringName(){
            String stringName = "";
            switch (this){
                case IVONA_TATYANA:
                    stringName = "Женский";
                    break;
                case IVONA_MAXIM:
                    stringName = "Мужской";
                    break;
                default:
            }
            return stringName;
        }
        public String getVoiceEnglishName(){
            String stringName = "";
            switch (this){
                case IVONA_TATYANA:
                    stringName = "tatyana";
                    break;
                case IVONA_MAXIM:
                    stringName = "maxim";
                    break;
                default:
            }
            return stringName;
        }
    }
    private TTSVoices ttsVoice;

    public void setTTSVoice(TTSVoices voice){
        this.ttsVoice = voice;
    }
    public TTSVoices getTTSVoice(){
        return this.ttsVoice;
    }


    /**
    /* Methods for setting alarms
    **/
    public boolean isTodayAlarmDay(){
        Calendar calendar = Calendar.getInstance();
        int[] table = {6,0,1,2,3,4,5};
        int todayOfWeek = table[calendar.get(Calendar.DAY_OF_WEEK)-1];
        return daysOfWeek[todayOfWeek];
    }
    // Returns alarm time in milliseconds
    public long getRunTimeMillis(){
        Calendar current = Calendar.getInstance();
        Calendar alarmTime = (Calendar) current.clone();

        // Set alarm time
        alarmTime.set(current.get(Calendar.YEAR),
                current.get(Calendar.MONTH),
                current.get(Calendar.DAY_OF_MONTH),
                this.hour,
                this.minute,
                0);
        alarmTime.set(Calendar.MILLISECOND, 0);

        // Convert days of week to russian format
        // and get today day of week
        int[] table = {6,0,1,2,3,4,5};
        int todayOfWeek = table[current.get(Calendar.DAY_OF_WEEK)-1];

        // Set alarm day if this is not today
        if (!(isTodayAlarmDay() && current.before(alarmTime))){
            int howMuchAdd = 0;
            int i, j;
            for (i = (todayOfWeek + 1) % 7 , j = 1; j <= 7; i = (i + 1) % 7, j++) {
                if (daysOfWeek[i]) {
                    howMuchAdd = j;
                    break;
                }
            }
            alarmTime.add(Calendar.DATE, howMuchAdd);
        }

//        Log.d("DEBUG", alarmTime.toString());
//        Log.d("DEBUG", current.toString());

        return alarmTime.getTimeInMillis();
    }
    // Returns time in milliseconds till alarm run
    private long getMillisTillRun(){
        int currentHour, currentMinute, currentSecond, currentMillisecond;
        long millisTillRun;
        final long DAY_MILLIS = 3600000 * 24;

        Calendar calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);
        currentSecond = calendar.get(Calendar.SECOND);
        currentMillisecond = calendar.get(Calendar.MILLISECOND);

        long currentMillisFrom00 = (currentHour * 3600 + currentMinute * 60 + currentSecond)
                * 1000 + currentMillisecond;
        long alarmMillisFrom00 = (this.hour * 3600 + this.minute * 60) * 1000;

        if (currentMillisFrom00 < alarmMillisFrom00){
            millisTillRun = alarmMillisFrom00 - currentMillisFrom00;
        }
        else{
            millisTillRun = alarmMillisFrom00 + (DAY_MILLIS - currentMillisFrom00);
        }
        return millisTillRun;
    }
    public long getDownloadStartTime(){
        final long DOWNLOAD_BEFORE = 600000;
        return System.currentTimeMillis()+ getMillisTillRun() - DOWNLOAD_BEFORE;
    }
    Intent createRingIntent(Context context){
        Intent ringIntent = new Intent(context, RingTheBell.class);
//        Log.d("DEBUG", "Melody id is " + String.valueOf(this.melody.getResId()));
//        ringIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ringIntent.putExtra("Melody", this.melody.getResId());
        ringIntent.putExtra("Volume", this.volumeLevel);
        // Put files names for the ringer
        for (TTSItemsNames item : getAllEnabledTTSItems()){
            String fileName = getFileName(item);
            if (!fileName.isEmpty()) {
                ringIntent.putExtra(item.toString(), fileName);
            }
        }
        return ringIntent;
    }
    private PendingIntent createRingPendingIntent(Context context, int id){
       Intent ringIntent = createRingIntent(context);
        return PendingIntent.getBroadcast(context, id, ringIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }
    public void cancelAlarm(Context context, int id){
        PendingIntent pi = createRingPendingIntent(context, id);
        pi.cancel();
    }
   public void setAlarm(Context context, int id){
       // Split alarm and download id's for comfortable logs reading
       int alarmID = id;
       int downloadID = id + 1000;
       final long DOWNLOAD_BEFORE = 600000;
       long millisTillRun = getMillisTillRun();
       long runAt = getRunTimeMillis();
       long downloadAt = runAt - DOWNLOAD_BEFORE;

//       Log.d("DEBUG", "new: " + String.valueOf(getRunTimeMillis()));
//       Log.d("DEBUG", "old: " + String.valueOf(runAt));

       // Create intent for alarm
        PendingIntent pendingRingIntent = createRingPendingIntent(context, id);

       AlarmManager ringAlarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

       // Schedule files download
       if (System.currentTimeMillis() >= downloadAt){
           downloadAt(context, System.currentTimeMillis(),  downloadID);
       }
       else{
           downloadAt(context, downloadAt, downloadID);
       }
       // Schedule alarm ring
       /**
       ** If API level >= 19 we have to use setExact() for ring at the precise time
       **/
       int currentapiVersion = android.os.Build.VERSION.SDK_INT;

       if (currentapiVersion >= Build.VERSION_CODES.KITKAT) { //API >= 19
           Log.d("DEBUG", "Alarm API >= 19 set");
           ringAlarm.setExact(AlarmManager.RTC_WAKEUP, runAt, pendingRingIntent);
       }
       else{ //API <= 18
           ringAlarm.set(AlarmManager.RTC_WAKEUP, runAt, pendingRingIntent);
           Log.d("DEBUG", "Alarm API <= 18 set");
       }

       Log.d("DEBUG",String.valueOf(this.hour) + ":" + String.valueOf(this.minute) +
               " was successfully set. ID: " + String.valueOf(alarmID));
       Log.d("DEBUG", "Seconds to alarm " + String.valueOf((runAt - System.currentTimeMillis())/1000));

    }
    public void downloadAt(Context context, long startTime, int id){
        Log.d("Debug", " Download ID: " + String.valueOf(id));
        Intent downloadIntent = new Intent(context, Downloader.class);
        downloadIntent.putExtra("alarmHour", this.hour);
        downloadIntent.putExtra("alarmMinute", this.minute);
        // Send this id for the reDownload purposes
        downloadIntent.putExtra("pendingIntentId", id);
        PendingIntent pendingDownloadIntent =
                PendingIntent.getBroadcast(context,
                        id,
                        downloadIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        // If it needs immidietly
        if (startTime <= System.currentTimeMillis()){
            context.sendBroadcast(downloadIntent);
            Log.d("DEBUG", "Download intent started manually");
        }
        else {
            AlarmManager ringAlarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.KITKAT) { //API >= 19
                ringAlarm.setExact(AlarmManager.RTC, startTime, pendingDownloadIntent);
            } else { //API <= 18
                ringAlarm.set(AlarmManager.RTC, startTime, pendingDownloadIntent);
//                Log.d("DEBUG", "startTime: " + String.valueOf(startTime) +
//                        " Current time: " + String.valueOf(System.currentTimeMillis()));
            }
            Log.d("DEBUG", "Download intent schedulled and will be perfomed in " +
            String.valueOf((startTime - System.currentTimeMillis()) / 1000) + " seconds");
        }


    }
    public String getDaysString(){
        String[] dayName = {"Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"};
        StringBuilder days = new StringBuilder();
        if (daysOfWeek[0] && daysOfWeek[1] && daysOfWeek[2] && daysOfWeek[3] && daysOfWeek[4]
                && !daysOfWeek[5] && !daysOfWeek[6]){
            days.append("По будням");
        }
        else if (daysOfWeek[0] && daysOfWeek[1] && daysOfWeek[2] && daysOfWeek[3] && daysOfWeek[4]
                && daysOfWeek[5] && daysOfWeek[6]){
            days.append("Каждый день");
        }
        else if (!daysOfWeek[0] && !daysOfWeek[1] && !daysOfWeek[2] && !daysOfWeek[3]
                && !daysOfWeek[4]&& daysOfWeek[5] && daysOfWeek[6]){
            days.append("По выходным");
        }
        else {
            for (int i = 0; i < daysOfWeek.length; i++) {
                if (daysOfWeek[i]) {
                    days.append(dayName[i]).append(" ");
                }
            }
        }
        return days.toString();
    }
    /*
    *   Generates files names for news, weather, etc. files
    *   Returns map with file type as a key element
    *   For news for example, key would be "news"
     */

   public synchronized String getFileName(TTSItemsNames item){
       StringBuilder fileName = new StringBuilder();

       String itemType = item.getItemType();
       fileName.append(itemType).append("_");

       if (itemType == "news"){
           fileName.append(item.getNewsProviderName()).append("_");
       }

       fileName.append(ttsVoice.getVoiceEnglishName());
       fileName.append(".mp3");

       return fileName.toString();
   }
    public synchronized String getURLParameters(TTSItemsNames item){
        StringBuilder paramaters = new StringBuilder();
        final String EMPTY_ANSWER = "";

        String itemType = item.getItemType();
        paramaters.append("?");
        paramaters.append("type=");
        paramaters.append(itemType).append("&");

        if (itemType == "news"){
            paramaters.append("provider=");
            paramaters.append(item.getNewsProviderName());
        }
        if (itemType == "gps"){
            LocationProvider provider = LocationProvider.getInstance();
            Location location = provider.getLocation();

            if (provider.hasPermissions() && location != null){
                Log.d("DEBUG", String.valueOf(location.getLatitude()));
                Log.d("DEBUG", String.valueOf(location.getLongitude()));
                paramaters.append("long=");
                paramaters.append(location.getLongitude());
                paramaters.append("&");
                paramaters.append("lat=");
                paramaters.append(location.getLatitude());
            }
            else {
                return EMPTY_ANSWER;
            }

        }
        paramaters.append("&voice=");
        paramaters.append(ttsVoice.getVoiceEnglishName());

        return paramaters.toString();
    }

}
