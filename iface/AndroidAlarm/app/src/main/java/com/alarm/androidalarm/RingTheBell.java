package com.alarm.androidalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class RingTheBell extends BroadcastReceiver {
    public RingTheBell() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent cancelIntent = new Intent(context, CancelAlarmActivity.class);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            cancelIntent.putExtras(extras);
        }
        cancelIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(cancelIntent);

        // This is a test for cancelling alarm from MainActivity
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pi);
    }
}
