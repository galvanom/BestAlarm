package com.alarm.androidalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;

public class Downloader extends BroadcastReceiver {
    public Downloader() {
    }

    @Override
    public void onReceive(final Context context, Intent intent) {

        Intent downloaderIntent = new Intent(context, DownloaderService.class);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            downloaderIntent.putExtras(extras);
        }
        context.startService(downloaderIntent);


    }
}
