package com.alarm.androidalarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class NotificationService extends Service {
    public static final String IS_SET = "IS_SET";
    private boolean isNotificationShown;
    private static final int notificationId = 0;
    private NotificationManager notificationManager;

    public NotificationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isNotificationShown = false;
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    }
    //BUG: Sometimes intent == null
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null){
            if (intent.hasExtra(IS_SET)){
                if (intent.getBooleanExtra(IS_SET, false)){
                    if (!isNotificationShown) {
                        showNotification();
                    }
                }
                else{
                    cancelNotification();
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }
    private void showNotification(){
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_alarm)
                .setContentTitle("Будильник установлен");
        Intent appIntent = new Intent(this, MainActivity.class);
        int pIntentId = 10000;
        PendingIntent pIntent =
                PendingIntent.getActivity(this, pIntentId, appIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        notificationBuilder.setContentIntent(pIntent);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

        notificationManager.notify(notificationId, notification);
        isNotificationShown = true;

    }
    private void cancelNotification(){
        notificationManager.cancel(notificationId);
        isNotificationShown = false;
    }
}
