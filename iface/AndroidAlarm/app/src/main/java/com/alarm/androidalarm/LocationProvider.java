package com.alarm.androidalarm;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.List;
public class LocationProvider {
    private Context context;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location location;
    private boolean hasPermissions = true;

    private static LocationProvider instance;
    public static void init(Context context){
        if (instance == null) {
            instance = new LocationProvider(context);
        }
    }
    public static synchronized LocationProvider getInstance(){
        return instance;
    }

    private LocationProvider(Context context) {
        this.context = context;
        locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new NetworkLocationListener();
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_DENIED ||
                ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_DENIED) {

            hasPermissions = false;
        }
        else{

            this.location = getLastKnownLocation();
            // Setting mock location
//            this.location = new Location(LocationManager.NETWORK_PROVIDER);
//            this.location.setLongitude(30.30);
//            this.location.setLatitude(59.80);
            //---------------------------------------------

            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                Log.d("DEBUG", "Network provider enabled");
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        3600 * 1000, 10000, locationListener);
            }
            if (locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,
                        10 * 1000, 10, locationListener);
                Log.d("DEBUG", "Passive provider enabled");
            }
        }
    }
    synchronized private Location getLastKnownLocation(){
        Location location = null;
        List<String> providers = locationManager.getAllProviders();
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            for (String provider : providers) {
//                Log.d("DEBUG", provider.toString());
                location = locationManager.getLastKnownLocation(provider);
                    if (location != null) {
                        break;
                }
            }
        }
        return  location;
    }
    public boolean isNetworkProviderEnabled(){
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
    public Location getLocation(){
        Location location = getLastKnownLocation();
        if (location != null){
            this.location = location;
        }
        return this.location;
    }
    public boolean hasPermissions(){ return hasPermissions; }

    private class NetworkLocationListener implements LocationListener{
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                LocationProvider.this.location = location;
            }
        }
        @Override
        public void onProviderDisabled(String provider) {

        }
        @Override
        public void onProviderEnabled(String provider) {

        }
    }

}
