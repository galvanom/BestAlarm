package com.alarm.androidalarm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PropertiesActivity extends AppCompatActivity {
    private static boolean isInternetCautionShown = false;
    private static boolean isGPSCautionShown = false;
    private int alarmNumber;
    private TimePicker timePicker;
    private AlertDialog daysOfWeekDialog;
    private AlertDialog melodiesDialog;
    private AlertDialog textToSpeechDialog;
    private AlertDialog ttsVoicesDialog;
    private Alarms alarms;
    private LocationProvider locationProvider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_properties);

        alarms = Alarms.getInstance();
        locationProvider = LocationProvider.getInstance();

        //Get alarm position from the main activity
        alarmNumber = getIntent().getIntExtra(Alarms.ALARM_NUMBER, -1);
        if (alarmNumber == -1){
            Log.d("DEBUG", "Can't get alarm number");
            this.finish();
        }

        // Set timepicker
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                alarms.putAlarmHour(alarmNumber, hourOfDay);
                alarms.putAlarmMinute(alarmNumber, minute);
            }
        });

        // Set volume level seekbar
        SeekBar volumeLevel = (SeekBar) findViewById(R.id.volumeLevel);
        volumeLevel.setMax(100);
        int volume = alarms.getAlarmVolume(alarmNumber);
        volumeLevel.setProgress(volume);
        // Get volume level text
        final TextView volumeLevelText = (TextView) findViewById(R.id.volumeLevelText);
        volumeLevelText.setText(getString(R.string.volume) + " " + volume + "%");
        volumeLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser){
                    alarms.putAlarmVolume(alarmNumber, progress);
                    volumeLevelText.setText(getString(R.string.volume) + " " + progress + "%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // Set days of week dialog
        initDaysOfWeekDialog(alarms.getAlarmDays(alarmNumber));
        // Set melodies dialog
        initMelodiesDialog();
        // Set TTS dialog
        initTextToSpeechDialog();
        //Set TTS Voices dialog
        initTTSVoicesDialog();
        //Set ListView items
        ArrayList<String> menuItems = new ArrayList<String>();
        menuItems.add(getString(R.string.properties_menu_days));//id = 0
        menuItems.add(getString(R.string.properties_menu_melody));
        menuItems.add(getString(R.string.properties_menu_tts));
        menuItems.add(getString(R.string.properties_menu_ttsvoice));
        // Set listeners for each ListView items
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0){
                    daysOfWeekDialog.show();
                }
                if (id == 1){
                    melodiesDialog.show();
                }
                if (id == 2){
                    textToSpeechDialog.show();
                    if (!isInternetCautionShown) {
                        Toast toast = Toast.makeText(
                                getApplicationContext(),
                                R.string.internet_caution,
                                Toast.LENGTH_LONG
                        );
                        toast.setGravity(Gravity.TOP, 0, 0);
                        toast.show();
                        isInternetCautionShown = true;
                    }
                }
                if (id == 3){
                    ttsVoicesDialog.show();
                }
            }
        };
        //Set adapter
        ListView alarmListView = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, menuItems);
        alarmListView.setAdapter(adapter);
        // Apply all items to current ListView
        alarmListView.setOnItemClickListener(itemClickListener);
        // If weather is enabled check for Google WiFi Location Settings
        if (alarms.getAlarmTTSItem(alarmNumber, AlarmItem.TTSItemsNames.OPENWEATHER)) {
            checkAndRequestLocationSettings();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        int hour = alarms.getAlarmHour(alarmNumber);
        int minute = alarms.getAlarmMinute(alarmNumber);

        // If activity is focused, draw time for particular AlarmItem
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            timePicker.setHour(hour);
            timePicker.setMinute(minute);
        }
        else{
            timePicker.setCurrentHour(hour);
            timePicker.setCurrentMinute(minute);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                // When changed cancel previous intent
                alarms.cancelAlarm(alarmNumber);
                // Set all alarms
                alarms.setAll();
                alarms.writeToDisk();
            }
        });

        Toast prefSaved = Toast.makeText(this, R.string.preferences_saved,Toast.LENGTH_SHORT);
        prefSaved.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void checkAndRequestLocationSettings(){
        if (locationProvider != null){
            if(!locationProvider.isNetworkProviderEnabled() && !isGPSCautionShown){
                AlertDialog wantToRedirect = new AlertDialog.Builder(this)
                        .setTitle(R.string.change_location_sett_title)
                        .setMessage(R.string.change_location_sett_fulltext)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectToLocationSettings();
                            }
                        })
                        .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create();
                        wantToRedirect.show();
                isGPSCautionShown = true;
            }
        }
    }
    private void redirectToLocationSettings(){
        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    private void initDaysOfWeekDialog(boolean[] whichDaysCheked){
        final CharSequence[] days = {"Пн","Вт","Ср","Чт","Пт","Сб","Вс"};
        DialogInterface.OnMultiChoiceClickListener listener =
        new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                Log.d("DEBUG", String.valueOf(which));
                alarms.putAlarmDay(alarmNumber, which, isChecked);
            }
        };
        daysOfWeekDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.properties_menu_days)
                .setMultiChoiceItems(days, whichDaysCheked, listener)
                .setNeutralButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();

    }
    private MediaPlayer melodyPlayer;
    private void stopMelody(){
        stopMediaPlayer(melodyPlayer);
        melodyPlayer = null;
    }
    private void stopMediaPlayer(MediaPlayer mp){
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
            mp.release();
        }
    }
    private void initMelodiesDialog(){
        int melodiesNumber = AlarmItem.Melodies.values().length;
        final AlarmItem.Melodies[] alarmMelodies = AlarmItem.Melodies.values();
        AlarmItem.Melodies checkedMelody = alarms.getAlarmMelody(alarmNumber);
        int checkedMelodyNumber = 0;


        CharSequence[] melodies = new CharSequence[melodiesNumber];
        for (int i = 0; i < melodiesNumber; i++){
            melodies[i] = alarmMelodies[i].toString();
            if (alarmMelodies[i] == checkedMelody){
                checkedMelodyNumber = i;
            }
        }
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlarmItem.Melodies melody = alarmMelodies[which];
                int melodyResource = melody.getResId();
                stopMediaPlayer(melodyPlayer);
                alarms.putAlarmMelody(alarmNumber, melody);
                melodyPlayer = MediaPlayer.create(getApplicationContext(), melodyResource);
                melodyPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopMelody();
                    }
                });
                melodyPlayer.start();
            }
        };

        melodiesDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.properties_menu_melody)
                .setSingleChoiceItems(melodies, checkedMelodyNumber, listener)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // Stop melody if playing
                        stopMelody();
                    }
                })
                .setNeutralButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Stop melody if playing
                        stopMelody();
                    }
                })
                .create();
    }
    private void initTextToSpeechDialog(){
        // Get String names and their boolean values
        int itemId = 0;
        final int itemsNumber = AlarmItem.TTSItemsNames.values().length;
        CharSequence[] ttsNames = new CharSequence[itemsNumber];
        boolean[] ttsCheckedArray = new boolean[itemsNumber];
        final AlarmItem.TTSItemsNames[] values = AlarmItem.TTSItemsNames.values();

        for (AlarmItem.TTSItemsNames itemName : values){
            ttsNames[itemId] = itemName.getStringName();
            ttsCheckedArray[itemId] = alarms.getAlarmTTSItem(alarmNumber, itemName);
            itemId++;
        }

        // Implement onClickListener
        DialogInterface.OnMultiChoiceClickListener listener =
                new DialogInterface.OnMultiChoiceClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        AlarmItem.TTSItemsNames name = values[which];
                        alarms.putAlarmTTSItem(alarmNumber, name, isChecked);

                    }
                };

        textToSpeechDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.properties_menu_tts)
                .setMultiChoiceItems(ttsNames, ttsCheckedArray, listener)
                .setNeutralButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();
    }
    private MediaPlayer voicePlayer;
    private void stopVoice(){
        stopMediaPlayer(voicePlayer);
        voicePlayer = null;
    }

    private void initTTSVoicesDialog(){
        final AlarmItem.TTSVoices[] voices = AlarmItem.TTSVoices.values();
        int voicesNumber = voices.length;
        CharSequence[] voiceNames = new CharSequence[voicesNumber];
        int checkedVoice = 0;
        int i = 0;

        for (AlarmItem.TTSVoices voice : voices){
            if (voice == alarms.getAlarmTTSVoice(alarmNumber)){
                checkedVoice = i;
            }
            voiceNames[i] = voice.getStringName();
            i++;
        }

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopMediaPlayer(voicePlayer);
                alarms.putAlarmTTSVoice(alarmNumber, voices[which]);
                int voiceResource = -1;
                if (voices[which] == AlarmItem.TTSVoices.IVONA_MAXIM){
                    voiceResource = R.raw.maxim;
                }
                if (voices[which] == AlarmItem.TTSVoices.IVONA_TATYANA){
                    voiceResource = R.raw.tatyana;
                }
                if (voiceResource != -1) {
                    voicePlayer = MediaPlayer.create(getApplicationContext(), voiceResource);
                    voicePlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            stopVoice();
                        }
                    });
                    voicePlayer.start();
                }
            }
        };

        ttsVoicesDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.properties_menu_ttsvoice)
                .setSingleChoiceItems(voiceNames, checkedVoice, listener)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        stopVoice();
                    }
                })
                .setNeutralButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        stopVoice();
                    }
                })
                .create();
    }
}
