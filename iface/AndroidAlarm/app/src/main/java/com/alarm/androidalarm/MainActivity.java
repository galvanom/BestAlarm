package com.alarm.androidalarm;

import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import java.util.ArrayList;
import android.util.Log;


public class MainActivity extends AppCompatActivity {

    ArrayList<AlarmItem> items;
    Alarms alarms;
    AlarmItemAdapter alarmItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alarms = Alarms.getInstance();
        items = alarms.getAlarmItems();
//        alarms.setAll();

        alarmItemAdapter = new AlarmItemAdapter(this, items);

        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(alarmItemAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        alarmItemAdapter.notifyDataSetChanged();
//        Log.d("DEBUG", "Resume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        alarmItemAdapter.notifyDataSetChanged();
//        Log.d("DEBUG", "Restart");
    }
    @Override
    protected void onPause() {
        super.onPause();
//        Log.d("DEBUG", "Main activity onPause");
        alarms.writeToDisk();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_buttons, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add_alarm){
            // Create new alarm and specify default hour and minute
            int alarmNumber = alarms.createNewAlarm(true, 6, 0);
            alarmItemAdapter.notifyDataSetChanged();
            alarmItemAdapter.callPropertiesActivity(alarmNumber);
        }
        return super.onOptionsItemSelected(item);
    }

    public void testButton(View view){

    }

}
