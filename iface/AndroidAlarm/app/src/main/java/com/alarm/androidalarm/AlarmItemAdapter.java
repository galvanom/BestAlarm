package com.alarm.androidalarm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.widget.BaseAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import java.util.ArrayList;
import java.util.StringTokenizer;


public class AlarmItemAdapter extends BaseAdapter {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<AlarmItem> items;
    Alarms alarms;

    public AlarmItemAdapter(Context context, ArrayList<AlarmItem> items){
        this.context = context;
        this.items = items;
        this.layoutInflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        alarms = Alarms.getInstance();

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item, parent, false);
        }

        AlarmItem item = getAlarmItem(position);
        View.OnClickListener textViewListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callPropertiesActivity(position);
//                deleteItem(position);
            }
        };
        int hour = items.get(position).getHour();
        int minute = items.get(position).getMinute();

        TextView timeText = (TextView) view.findViewById(R.id.largeText);
        TextView daysText = (TextView) view.findViewById(R.id.smallText);
        View coverView = (View) view.findViewById(R.id.coverView);
        timeText.setText(hour + ":" + getNumberWithZeroString(minute));

        coverView.setOnClickListener(textViewListener);

        View.OnCreateContextMenuListener contextMenuListener = new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add("Редактировать").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        callPropertiesActivity(position);
                        return false;
                    }
                });
                menu.add("Удалить").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        alarms.putAlarmChecked(position, false);
                        alarms.cancelAlarm(position);
                        alarms.deleteAlarm(position);
                        notifyDataSetChanged();
                        return false;
                    }
                });
                menu.add("Запустить").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        alarms.runAlarm(position);
                        return false;
                    }
                });
            }
        };

        coverView.setOnCreateContextMenuListener(contextMenuListener);

        ((TextView) view.findViewById(R.id.smallText)).setText(item.getDaysString());

        view.setBackgroundColor(ContextCompat.getColor(context, R.color.mainItem));

        Switch sw = (Switch) view.findViewById(R.id.switch2);
        sw.setTag(position);
        sw.setChecked(item.isChecked());
        sw.setOnCheckedChangeListener(myCheckChangeList);

        return view;
    }

    AlarmItem getAlarmItem(int position) {
        return ((AlarmItem) getItem(position));
    }

        OnCheckedChangeListener myCheckChangeList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {

            //Log.d("DEBUG", String.valueOf(buttonView.getTag()));
            int alarmNumber = (int) buttonView.getTag();
            alarms.putAlarmChecked(alarmNumber, isChecked);
            if (!isChecked) {
                Log.d("DEBUG", "Alarm " + String.valueOf(alarmNumber) + " cancelled");
                alarms.cancelAlarm(alarmNumber);
            }
            else{
                Log.d("DEBUG", "Alarm " + String.valueOf(alarmNumber) + " set");
                alarms.setAlarm(alarmNumber);
            }
            alarms.writeToDisk();
        }
    };
//    void deleteItem(int position){
//        alarms.deleteAlarm(position);
//        this.notifyDataSetChanged();
//    }
    public void callPropertiesActivity(int position){
        Intent intent = new Intent(context, PropertiesActivity.class);
        intent.putExtra(Alarms.ALARM_NUMBER, position);
        context.startActivity(intent);
    }
    // Add 0 to 7 = 07
    private String getNumberWithZeroString(int number){
        StringBuilder value = new StringBuilder();
        if (number < 10){
            value.append("0").append(number);
        }
        else{
            value.append(number);
        }
        return value.toString();
    }

}
