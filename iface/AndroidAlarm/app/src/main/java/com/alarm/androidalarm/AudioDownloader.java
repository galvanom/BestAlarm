package com.alarm.androidalarm;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.net.URL;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.ListIterator;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;


public class AudioDownloader{
    private static final int CONNECTION_OK = 200;

    private HttpURLConnection getServerConnection(String url){
        HttpURLConnection connection = null;
        try{
            URL fullURL = new URL(url);

            connection = (HttpURLConnection) fullURL.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
        }
        catch(IOException e){
            Log.d("DEBUG", "Can't connect to server. " + e.toString());
        }
        return connection;
    }
    private InputStream getServerFileStream(HttpURLConnection connection){
        InputStream in = null;
        if (connection != null) {
            String contentType = connection.getContentType();
            try {
                Log.d("DEBUG", "Content type: " + contentType);
                if (connection.getResponseCode() == CONNECTION_OK
                        && contentType != null
                        && contentType.contains("audio")) {
                    in = connection.getInputStream();
                }
            }
            catch(IOException e){
                Log.d("DEBUG", "Can't get file stream from server " + e.toString());
            }
        }
        return in;
    }
    private WifiLock wifiLock;

    public boolean getFileAndSave(Context context, String url, String path) throws IOException{
        int fileSize = 0;
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm == null){
            Log.d("DEBUG", "Connectivity manager is null");
        }
        if (!lockWiFi(context)){
            Log.d("DEBUG", "Couldn't force Wifi connection");
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
            HttpURLConnection connection = getServerConnection(url);
            InputStream in = getServerFileStream(connection);
            if (connection ==  null || in == null){
                Log.d("DEBUG", "Get file from server failed. Url: " + url);
                wifiLockRelease();
                return  false;
            }
            fileSize = connection.getContentLength();
            Log.d("DEBUG", "File size: " + String.valueOf(fileSize) + " bytes");

            if (path == null || path.isEmpty()) {
                throw new IOException("Path is incorrect\n");
            }
            try {
                File outputFile = new File(context.getFilesDir(), path);
                FileOutputStream outputStream = new FileOutputStream(outputFile);
                byte[] buffer = new byte[1024];
                int bytesRead = in.read(buffer);
                int totalBytesRead = 0;
                if (bytesRead <= 0){
                    outputStream.close();
                    outputFile.delete();
                    wifiLockRelease();
                    return false;
                }

                while (bytesRead > 0) {
                    outputStream.write(buffer, 0, bytesRead);
                    totalBytesRead += bytesRead;
                    bytesRead = in.read(buffer);
                }
                outputStream.close();
                connection.disconnect();

                // if Content length was set, check amount of bytes read
                if (fileSize > 0) {
                    if (totalBytesRead  < fileSize){
                        Log.d("DEBUG", "Bytes read are less then the content length: " +
                                String.valueOf(totalBytesRead) + " " + String.valueOf(fileSize));
                        // delete corrupted file
                        if (outputFile.delete()){
                            Log.d("DEBUG", "File deleted");
                        }
                        else{
                            Log.d("DEBUG", "File can not be deleted");
                        }
                        wifiLockRelease();
                        return false;
                    }
                }

            } catch (IOException e) {
                System.out.println(e.toString());
                wifiLockRelease();
                return false;
            }
        }
        else {
            Log.d("DEBUG", "Default network is unavailable or couldn't connect to network");
            wifiLockRelease();
            return false;
        }

        wifiLockRelease();
        return true;
    }
    private void wifiLockRelease(){
        if (wifiLock != null){
            if (wifiLock.isHeld()){
                wifiLock.release();
            }
        }
    }
    private boolean lockWiFi(Context context){
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "Lock tag");
            if (wifiLock != null){
                wifiLock.acquire();
                return true;
            }
        }
        return false;
    }
}


