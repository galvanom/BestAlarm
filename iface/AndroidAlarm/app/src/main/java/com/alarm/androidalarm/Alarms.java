package com.alarm.androidalarm;


import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class Alarms{
    private static Alarms instance;

    private android.content.Context context;
    private Gson gson;
    private ArrayList<AlarmItem> alarmList;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor preferencesEditor;

    private static final String GLOBAL_ALARM_SETTINGS = "GLOBAL_ALARM_SETTINGS";
    protected static final String ALARM_NUMBER = "ALARM_NUMBER";

    public static synchronized Alarms getInstance() {
        return instance;
    }

    public static void init(android.content.Context context){
        if (instance == null) {
            instance = new Alarms(context);
        }
    }

    private Alarms(Context context) {
        this.context = context;
        alarmList = new ArrayList<AlarmItem>();
        gson = new Gson();

        sharedPreferences = this.context.getSharedPreferences(GLOBAL_ALARM_SETTINGS, MODE_PRIVATE);
        preferencesEditor = sharedPreferences.edit();

        readFromDisk();
    }
    void setAll(){
        int id = 0;
        for (AlarmItem item :  alarmList){
            if (item.isChecked()) {
                item.setAlarm(context, id);
            }
            id++;
        }
    }
    public void setAlarm(int alarmNumber){
        AlarmItem item = alarmList.get(alarmNumber);
        if (item != null){
            item.setAlarm(this.context, alarmNumber);
        }
    }
    public void cancelAlarm(int id){
        AlarmItem item = alarmList.get(id);
        if (item != null){
            item.cancelAlarm(this.context, id);
        }
    }
    private void readFromDisk() {
        final int elementsNumber = sharedPreferences.getAll().size();
        for (int i = 0; i < elementsNumber; i++){
            alarmList.add(null);
        }

        for (Map.Entry entry: sharedPreferences.getAll().entrySet()){
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            alarmList.set(Integer.parseInt(key), gson.fromJson(value, AlarmItem.class));
        }
    }
    public int createNewAlarm(boolean isChecked, int hour, int minute){
        int alarmNumber;
        // Create new alarm object
        AlarmItem item = new AlarmItem(isChecked, hour, minute);
        alarmList.add(item);
        alarmNumber = alarmList.size() - 1;

        // Convert new alarm object to JSON
        // and write it to the SharedPreferences
        applyAlarm(alarmNumber);
//        String serialized = gson.toJson(item);
//        preferencesEditor.putString(String.valueOf(alarmNumber), serialized);
        writeToDisk();

        changeAlarmNotification();

        return alarmNumber;
    }
    public void deleteAlarm(int alarmNumber){
        // Remove item from collection
        alarmList.remove(alarmNumber);
        //Remove item from shred preferences
        preferencesEditor.remove(String.valueOf(alarmNumber));
        // Write all items to editor
        applyAll();
        // Commit changes
        writeToDisk();
    }
    private void applyAll(){
        preferencesEditor.clear().apply();
        for (int i = 0; i < alarmList.size(); i++){
            applyAlarm(i);
        }
    }
    // Change hour
    public void putAlarmHour(int alarmNumber, int hour){
        alarmList.get(alarmNumber).setHour(hour);
        applyAlarm(alarmNumber);
    }
    public int getAlarmHour(int alarmNumber){
        int hour = alarmList.get(alarmNumber).getHour();
        return hour;
    }
    // Change minute
    public void putAlarmMinute(int alarmNumber, int minute){
        alarmList.get(alarmNumber).setMinute(minute);
        applyAlarm(alarmNumber);
    }
    public int getAlarmMinute(int alarmNumber){
        int minute = alarmList.get(alarmNumber).getMinute();

        return minute;
    }
    // Change isCheked
    public void putAlarmChecked(int alarmNumber, boolean isChecked){
        alarmList.get(alarmNumber).setChecked(isChecked);
        applyAlarm(alarmNumber);
        changeAlarmNotification();
    }
    public boolean getAlarmChecked(int alarmNumber){
        boolean isChecked = alarmList.get(alarmNumber).isChecked();
        return isChecked;
    }
    public void putAlarmDay(int alarmNumber, int dayNumber, boolean value){
        alarmList.get(alarmNumber).setDaysOfWeek(dayNumber, value);
        applyAlarm(alarmNumber);
    }
    public boolean[] getAlarmDays(int alarmNumber){
        return alarmList.get(alarmNumber).getDaysOfWeek();
    }
    public void putAlarmMelody(int alarmNumber, AlarmItem.Melodies melody){
        alarmList.get(alarmNumber).setMelody(melody);
        applyAlarm(alarmNumber);
    }
    public AlarmItem.Melodies getAlarmMelody(int alarmNumber){
        return alarmList.get(alarmNumber).getMelody();
    }
    public ArrayList<AlarmItem> getAlarmItems(){
        return alarmList;
    }
    public void putAlarmTTSItem(int alarmNumber, AlarmItem.TTSItemsNames name, Boolean value){
        alarmList.get(alarmNumber).setTTSItem(name, value);
        applyAlarm(alarmNumber);
    }
    public Boolean getAlarmTTSItem(int alarmNumber, AlarmItem.TTSItemsNames name){
        return alarmList.get(alarmNumber).getTTSItem(name);
    }
    public void putAlarmTTSVoice(int alarmNumber, AlarmItem.TTSVoices voice){
        alarmList.get(alarmNumber).setTTSVoice(voice);
        applyAlarm(alarmNumber);
    }
    public AlarmItem.TTSVoices getAlarmTTSVoice(int alarmNumber){
        return alarmList.get(alarmNumber).getTTSVoice();
    }
    public void putAlarmVolume(int alarmNumber, int volume){
        alarmList.get(alarmNumber).setVolumeLevel(volume);
        applyAlarm(alarmNumber);
    }
    public int getAlarmVolume(int alarmNumber){
        return alarmList.get(alarmNumber).getVolumeLevel();
    }
    private void applyAlarm(int alarmNumber){
        String serialized = gson.toJson(alarmList.get(alarmNumber));
        preferencesEditor.putString(String.valueOf(alarmNumber), serialized);
    }
    public void writeToDisk(){
        preferencesEditor.apply();
        Log.d("DEBUG", "Changes are committed");
    }
    private boolean doesAnyAlarmSet(){
        // Set notification if any alarm was set
        boolean isSet = false;
        for (AlarmItem item : alarmList){
            if (item.isChecked()){
                isSet = true;
                break;
            }
        }
        return isSet;
    }
    public void changeAlarmNotification(){
        Intent notificationIntent = new Intent(this.context, NotificationService.class);
        notificationIntent.putExtra(NotificationService.IS_SET, doesAnyAlarmSet());
        this.context.startService(notificationIntent);
    }
    public void runAlarm(int alarmNumber){
        Intent ringIntent = alarmList.get(alarmNumber).createRingIntent(this.context);
        this.context.sendBroadcast(ringIntent);
    }
}
