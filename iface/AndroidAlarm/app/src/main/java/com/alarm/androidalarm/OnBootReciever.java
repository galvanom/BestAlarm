package com.alarm.androidalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;

public class OnBootReciever extends BroadcastReceiver {
    public OnBootReciever() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == Intent.ACTION_BOOT_COMPLETED){
            Alarms alarms = Alarms.getInstance();
            alarms.setAll();

            // Show notification
            alarms.changeAlarmNotification();
        }
    }

}
