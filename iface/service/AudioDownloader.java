package iface.service;

import java.net.HttpURLConnection;
import java.net.URL;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;

public class AudioDownloader{
	public static void main(String args[]){
		try{
			new AudioDownloader().getFileAndSave("http://127.0.0.1/index.php","type=news","news.mp3");
		}
		catch (IOException e){
			System.out.println(e);
		}
		
	}
	private static final int CONNECTION_OK = 200;
	
	public InputStream getFileFromServer(String url, String params){
		InputStream in = null;
		try{
			URL fullURL = new URL(url + "?" + params);
			
			HttpURLConnection connection = (HttpURLConnection) fullURL.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			if (connection.getResponseCode() == CONNECTION_OK){
				in = connection.getInputStream();
			}
		}
		catch(IOException e){
			System.out.println(e);
		}
		return in;
	}
	public void getFileAndSave(String url, String params, String path)throws IOException{
		
		InputStream in = getFileFromServer(url, params);

		if (path == null || path.length() == 0){
			throw new IOException("Path is incorrect\n");
		}
		try {
			FileOutputStream outputFile = new FileOutputStream(new File(path));
			byte[] buffer = new byte[1024];
			int bytesRead;

			while((bytesRead = in.read(buffer)) > 0){
				outputFile.write(buffer, 0, bytesRead);

			}
		}
		catch (IOException e){
			System.out.println(e);
		}

	}
}